## Plot density of CCFs and represents cluster informations


### Install

```r
library(devtools)

install_bitbucket("weischenfeldt/dens_clust")
```

### load Load the package and test data

```r
library(densClust)

data(ccfs_22)
```

### Plot basic density with squash::colorgram

```r
g_dens <- grid_dens(ccfs_22[,1], ccfs_22[,2], 10000)

library(squash)

colFn = colorRampPalette(c("white", "yellow", "red"))
map <- makecmap(c(0, max(g_dens$z)), colFn = colFn, include.lowest = TRUE)
colorgram(g_dens, outlier = NA, map = map, zlab = "Absolute density",
   main = "CCF density tum_22 vs met_22",
   xlab = "met_22 CCF", ylab = "tum_22 CCF", las = 1, bty = "n")
abline(h=c(0, 0.5, 1, 1.5), col = "lightgray", lty = "dotted",
       lwd = par("lwd"))
abline(v=c(0, 0.5, 1, 1.5), col = "lightgray", lty = "dotted",
       lwd = par("lwd"))
```
![dens](figures/dens_ccfs.png)


### Plot contours

```r
plot_dens_contours(g_dens, add = T, likThresh = c(0.75, 0.5, 0.25),
   col = c("black", "blue", "red"))
```
![dens_cont](figures/dens_ccfs_cont.png)



### Visualize simplified clusters


```r
plot_ellipses(ccfs_22[,1], ccfs_22[,2] , ccfs_22[,3],
    main = "CCF clusters tum_22 vs met_22",
    xlab = "met_22 CCF", ylab = "tum_22 CCF", las = 1, bty = "n")
abline(h=c(0, 0.5, 1, 1.5), col = "lightgray", lty = "dotted",
    lwd = par("lwd"))
abline(v=c(0, 0.5, 1, 1.5), col = "lightgray", lty = "dotted",
    lwd = par("lwd"))
plot_dens_contours(g_dens, add = T, likThresh = c(0.75, 0.5, 0.25),
    col = c("black", "blue", "red"))

```
![elliplot](figures/elliplot_ccfs.png)
